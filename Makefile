src: 
	$(MAKE) -C $@

src-clean:
	$(MAKE) -C $(@:%-clean=%) clean

test:
	$(MAKE) -C $@

test-clean:
	$(MAKE) -C $(@:%-clean=%) clean

test-coverage: test
	$(MAKE) -C $(@:%-coverage=%) coverage

all: src test

coverage: test-coverage

clean: src-clean test-clean
	rmdir build

.PHONY: all src test clean coverage src-clean test-clean test-coverage
