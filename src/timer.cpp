#include <stdexcept>
#include "timer.hpp"
#include "logger.hpp"
#include "engine.hpp"
#include <sys/timerfd.h>
#include <sys/epoll.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include "utils.hpp"

namespace Sb {
    Timer::Timer(int64_t timeoutNanoSecs) :
        timeoutNanoSecs(timeoutNanoSecs) {
        fd = timerfd_create (CLOCK_MONOTONIC, TFD_NONBLOCK);
        if (fd < 0) {
              throw std::runtime_error(::strerror(errno));
        }
        itimerspec new_timer;
        new_timer.it_interval.tv_sec = 0;
        new_timer.it_interval.tv_nsec = 0;
        new_timer.it_value.tv_sec = timeoutNanoSecs / NanoSecsInSecs;
        new_timer.it_value.tv_nsec = timeoutNanoSecs % NanoSecsInSecs;
        itimerspec old_timer;
        if(::timerfd_settime(fd, 0, &new_timer, &old_timer) < 0) {
            throw std::runtime_error(::strerror(errno));
        }
    }

    Timer::~Timer() {
        Cancel();
        Logger::Log(Logger::LogType::INFO, "Timer::~Timer()");
    }

    void Timer::Run() {
        Logger::Log(Logger::LogType::INFO, "Timer::Run");
    }

    int64_t Timer::GetTimeoutNanoSecs() const {
        return timeoutNanoSecs;
    }

//    void Timer::ResetTimer() {
        // add event to be triggered
//        itimerspec new_timer;
//        new_timer.it_interval.tv_sec = 0;
//        new_timer.it_interval.tv_nsec = 0;
//        new_timer.it_value.tv_sec = timeoutSecs;
//        new_timer.it_value.tv_nsec = 0;
//        itimerspec old_timer;
//        if(::timerfd_settime(fd, 0, &new_timer, &old_timer) < 0) {
//            throw std::runtime_error(::strerror(errno));
//        }
//    }
}
