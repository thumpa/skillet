#include "epollable.hpp"

namespace Sb {
    Epollable::~Epollable() {
    }

    void Epollable::Cancel() {
    }

    int Epollable::GetFd() const {
        return fd;
    }
}
