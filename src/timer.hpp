#pragma once

#include "engine.hpp"

namespace Sb {
    class Timer : virtual public Epollable {
    public:
        Timer(int64_t timeoutNanoSecs);
        virtual ~Timer();
        int64_t GetTimeoutNanoSecs() const;
        virtual void Run();
    private:
        void ResetTimer(int64_t timeoutNanoSecs);
        const int64_t timeoutNanoSecs;
    };
}
