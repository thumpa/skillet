#pragma once

#include <queue>
#include <semaphore.h>
#include <thread>
#include <map>
#include <mutex>
#include <vector>
#include "epollable.hpp"
#include "stats.hpp"

namespace Sb {
    class Engine final {
    public:
        static void Go(int minWorkersPerCpu = 2);
        static void Add(Epollable*);
        static void AddRoot(Epollable*);
        static void RunIn(Epollable*, int64_t whenNs);
        static void RunInRoot(Epollable*, int64_t whenNs);
        ~Engine();
    private:
        class Thread {
        public:
            Stats stats;
            bool exited = false;
            std::thread::native_handle_type handle = 0;
        };
    private:
        Engine();
        static void DoEpoll(Thread* me) noexcept;
        static void DoWork(Thread* me) noexcept;
        static void SignalHandler(int signalNum);
        static void EnableSigIntHandler();
        void Stop();
        void Doit(int minWorkersPerCpu);
        void DoMaster();
        void Epoll(Thread& me);
        void Work(Thread& me);
        void BlockStart();
        void DoAddEpoll(Epollable* run);
        void DoAddRoot(Epollable* run);
        void DoRunIn(Epollable* run, int64_t whenNs);
        void DoRunInRoot(Epollable* run, int64_t whenNs);
        void Push(Epollable *);
        void Interrupt(Thread& thread) const;
        std::thread::id GetMasterThreadId() const;
        Epollable* Pop();
    private:
        static Engine* theEngine;
        volatile bool stopped = false;
        int epollFd = -1;
        std::mutex lock;
        sem_t wait;
        std::queue<Epollable*> queue;
        std::vector<Epollable*> roots;
        const std::thread::id masterThreadId;
        std::vector<std::unique_ptr<Thread>> slaves;
    private:
        const NanoSecs MAX_CPU_RUN_IN_NSECS = NanoSecs(5000000000);
        const NanoSecs SHUTDOWN_WAIT_CHECK_NSECS = NanoSecs(100000000);
        const int MAX_SHUTDOWN_ATTEMPTS = 5;
        const int MASTER_POLL_TIME_SECS = 1;
        const int EPOLL_QUEUE_LEN = 32768;
        const int EPOLL_EVENTS_PER_RUN = 32;
    };
}
