#pragma once

#include <iostream>
#include <string>
#include "engine.hpp"
#include <mutex>
#include <thread>
#include <vector>
#include <limits>

namespace Sb {
    class Logger final {
    friend class Engine;
    public:
        enum class LogType {
            NOTHING = 0,
            ERROR,
            WARNING,
            INFO,
            EVERYTHING
        };
        static void Log(LogType type, std::string what);
        static void SetMask(LogType mask);
    private:
        Logger();
        void DoLog(LogType type, std::string what);
        void DoSetMask(LogType mask);
        static void Start();
        static void Stop();
    private:
        std::vector<std::thread::id> threadIds;
        std::mutex lock;
        LogType logMask = LogType::EVERYTHING;
        static Logger* theLogger;
    };
}
