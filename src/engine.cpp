#include <queue>
#include <memory>
#include <stdexcept>
#include <sys/epoll.h>
#include <sys/timerfd.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include "engine.hpp"
#include "utils.hpp"
#include "logger.hpp"
#include <unistd.h>
#include <pthread.h>

namespace Sb {
    Engine* Engine::theEngine = nullptr;

    void Engine::SignalHandler(int signalNum) {
        Logger::Log(Logger::LogType::INFO, "Got signal " + IntToString(signalNum));
        if(Engine::theEngine->GetMasterThreadId() == std::this_thread::get_id() && signalNum != SIGCONT) {
            Logger::Log(Logger::LogType::INFO, "Got SIGINT on main thread. Stopping.");
            Engine::theEngine->Stop();
        }
    }

    Engine::Engine() :
            masterThreadId(std::this_thread::get_id()) {
        Logger::Start();

        epollFd = ::epoll_create(EPOLL_QUEUE_LEN);
        if(epollFd < 0) {
            throw std::runtime_error(::strerror(errno));
        }
    }

    Engine::~Engine() {
        ::sem_destroy(&wait);
        struct sigaction sigAction;
        sigAction.sa_flags = 0;
        sigAction.sa_handler = SIG_IGN;
        sigemptyset(&sigAction.sa_mask);
        ::sigaction(SIGINT, &sigAction, nullptr);
        for(auto* root: roots) {
            delete root;
        }
        if(epollFd >= 0) {
            ::close(epollFd);
        }
        Logger::Log(Logger::LogType::INFO, "Engine destroyed. Please call Init() before Go().");
        Logger::Stop();
    }


    std::thread::id Engine::GetMasterThreadId() const {
        return masterThreadId;
    }

    void Engine::Go(int minWorkersPerCpu) {
        std::unique_ptr<Engine> tmp(Engine::theEngine);
        theEngine->Doit(minWorkersPerCpu);
        Engine::theEngine = nullptr;
    }

    void Engine::Doit(int minWorkersPerCpu) {
        assert(theEngine != nullptr, "Need to Add() something before Go().");
        struct sigaction sigAction;
        sigAction.sa_handler = Engine::SignalHandler;
        sigAction.sa_flags = SA_RESTART;
        ::sigemptyset(&sigAction.sa_mask);
        ::sigaction(SIGINT, &sigAction, nullptr);
        int initialNumThreadsToSpawn = std::thread::hardware_concurrency() * minWorkersPerCpu;
        Logger::Log(Logger::LogType::INFO, "Going to spawn " + std::to_string(initialNumThreadsToSpawn));

        ::sem_init(&wait, 0, 0);
        lock.lock();
        std::vector<std::thread> threads;
        for(int i = 0; i < initialNumThreadsToSpawn; i++) {
            std::unique_ptr<Thread> worker(new Thread);
            if(i == 0) {
                threads.push_back(std::thread(Engine::DoEpoll, worker.get()));
            } else {
                threads.push_back(std::thread(Engine::DoWork, worker.get()));
            }
            worker.get()->handle = threads.back().native_handle();
            threads.back().detach();
            slaves.push_back(std::move(worker));
        }
        lock.unlock();
        DoMaster();

        auto waiting = true;
        for(auto i = MAX_SHUTDOWN_ATTEMPTS; i > 0 && waiting; i--) {
            for(auto& slave: slaves) {
                ::sem_post(&wait);
                Interrupt(*slave.get());
            }

            waiting = false;
            for(auto& slave: slaves) {
                if(!slave.get()->exited) {
                    waiting = true;
                }
            }
            std::this_thread::sleep_for(NanoSecs(SHUTDOWN_WAIT_CHECK_NSECS));
        }
        Logger::Log(Logger::LogType::INFO, "All done, exiting.");
    }

    void Engine::Push(Epollable* run) {
        std::lock_guard<std::mutex> sync(lock);
        (void)sync;
        queue.push(run);
    }

    Epollable* Engine::Pop() {
        std::lock_guard<std::mutex> sync(lock);
        (void)sync;
        Epollable* ret = nullptr;
        if(queue.size() > 0) {
            ret = queue.front();
            queue.pop();
        }
        return ret;
    }

    void Engine::DoAddEpoll(Epollable* run) {
        epoll_event event = { 0, { 0 } };
        event.events = EPOLLET | EPOLLIN | EPOLLOUT | EPOLLERR;
        event.data.ptr = run;
        if(::epoll_ctl (epollFd, EPOLL_CTL_ADD, run->GetFd(), &event) < 0) {
            throw std::runtime_error(::strerror(errno));
        }
    }

    void Engine::DoAddRoot(Epollable* run) {
        roots.push_back(run);
        DoAddEpoll(run);
    }


    void Engine::AddRoot(Epollable* root) {
        if(Engine::theEngine == nullptr) {
            Engine::theEngine = new Engine();
        }
        theEngine->DoAddRoot(root);
    }

    void Engine::Add(Epollable* run) {
        if(Engine::theEngine == nullptr) {
            Engine::theEngine = new Engine();
        }
        theEngine->DoAddEpoll(run);
    }

    void Engine::Stop() {
        stopped = true;
    }

    void Engine::DoMaster() {
        Logger::Log(Logger::LogType::INFO, "All threads started.");

        while(!stopped) {
            std::this_thread::sleep_for(Seconds(MASTER_POLL_TIME_SECS));
            if(stopped) {
                break;
            }
            for(auto& slave: slaves) {
                if(slave.get()->stats.GetBusyTimeNs() > MAX_CPU_RUN_IN_NSECS) {
//                    Logger::Log(Log::)
                    Interrupt(*slave.get());
                }
            }
        }
    }

    void Engine::BlockStart() {
        std::lock_guard<std::mutex> sync(lock);
        (void)sync;
    }

    void Engine::Work(Thread& me) {
        try {
            BlockStart();
            Logger::Log(Logger::LogType::INFO, "Starting Work");
            while(!stopped) {
                me.stats.MarkIdleStart();
                ::sem_wait(&wait);
                me.stats.MarkIdleEnd();
                if(stopped) {
                    break;
                }
                auto run = Pop();
                Logger::Log(Logger::LogType::INFO, "Running Work");
                if(run != nullptr) {
                    run->Run();
                }
            }
        } catch(std::exception& e) {
            Logger::Log(Logger::LogType::ERROR, std::string("Worker Thread threw a ") + e.what());
            Engine::theEngine->Stop();
        } catch(...) {

            Logger::Log(Logger::LogType::ERROR, "Unknown exception in Engine::DoWork");
            Engine::theEngine->Stop();
        }
        Logger::Log(Logger::LogType::INFO, "Stopping Work");
        me.exited = true;
    }

    void Engine::DoWork(Thread *me) noexcept {
        theEngine->Work(*me);
    }

    void Engine::Epoll(Thread& me) {
        try {
            BlockStart();
            Logger::Log(Logger::LogType::INFO, "Starting Epoll");
            while(!stopped) {
                epoll_event events[EPOLL_EVENTS_PER_RUN];
                me.stats.MarkIdleStart();
                int num = epoll_wait(epollFd, events, sizeof(events)/sizeof(events[0]), -1);
                me.stats.MarkIdleEnd();
                if(stopped) {
                    break;
                }
                if(num < 0) {
                    if(errno == EINTR) {
                        Logger::Log(Logger::LogType::INFO, "epoll_wait interrupted.");
                    } else {
                        Logger::Log(Logger::LogType::INFO, "Perror " + IntToString(errno) + " " + ::strerror(errno));
                        throw std::runtime_error(::strerror(errno));
                    }
                } else {
                    for(int i = 0; i < num; i++) {
                        Epollable* what = static_cast<Epollable*>(events[i].data.ptr);
                        Push(what);
                        ::sem_post(&wait);
                    }
                }
            }
        } catch(std::exception& e) {
            Logger::Log(Logger::LogType::ERROR, "Unknown exception in Engine::DoWork");
            Engine::theEngine->Stop();
        } catch(...) {
            Logger::Log(Logger::LogType::ERROR, "Unknown exception in Engine::DoEpoll");
            Engine::theEngine->Stop();
        }
        Logger::Log(Logger::LogType::INFO, "Stopping Epoll");
        me.exited = true;
    }

    void Engine::DoEpoll(Thread* me) noexcept {
        theEngine->Epoll(*me);
    }

    void Engine::Interrupt(Thread &thread) const {
        if(GetMasterThreadId() != std::this_thread::get_id()) {
            throw std::logic_error("Cannot call interrupt from non WatchDog thread.");
        }
        if(!thread.exited && thread.handle != 0) {
            Logger::Log(Logger::LogType::INFO, "Interrupting thread");
            ::pthread_kill(thread.handle, SIGINT);
        }
    }
}
