#pragma once
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <chrono>

namespace Sb {
    const int64_t NanoSecsInSecs = { 1000000000 };

    typedef std::chrono::nanoseconds NanoSecs;
    typedef std::chrono::seconds Seconds;
    typedef std::chrono::high_resolution_clock HighResClock;

    inline void assert(bool ok, const char error[]) {
        if(!ok) {
            throw std::runtime_error(error);
        }
    }

    template<typename T, typename U>
    inline auto Elapsed(const T& end, const U& start) -> decltype(end - start) {
        return end - start;
    }

    inline std::string IntToString(int number, int width = -1) {
        std::stringbuf buf;
        std::ostream os(&buf);
        if(width > 0) {
            os << std::setw(width);
        }
        os << number;
        return buf.str();
    }
}

