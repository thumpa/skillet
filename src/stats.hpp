#pragma once

#include "utils.hpp"

namespace Sb {
    class Stats final {
    public:
        Stats();
        void MarkIdleStart();
        void MarkIdleEnd();
        void Notify(int num);
        NanoSecs GetBusyTimeNs() const;
    private:
        std::chrono::time_point<HighResClock, NanoSecs> creation;
        std::chrono::time_point<HighResClock, NanoSecs> mark;
        NanoSecs idleNs;
        NanoSecs busyNs;
        int counter;
        bool active;
    };

}
