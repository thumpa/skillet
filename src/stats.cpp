#include "utils.hpp"
#include "stats.hpp"


namespace Sb {
    Stats::Stats() :
        creation(HighResClock::now()),
        mark(HighResClock::now()),
        idleNs(0),
        busyNs(0),
        counter(0),
        active(true)
    {
    }

    void Stats::MarkIdleStart() {
        assert(active, "Already active.");
        active = false;
        auto now = HighResClock::now();
        idleNs += Elapsed(now, mark);
        mark = now;
    }

    void Stats::MarkIdleEnd() {
        assert(!active, "Not activated yet.");
        active = true;
        auto now = HighResClock::now();
        busyNs += Elapsed(now, mark);
        mark = now;
    }

    NanoSecs Stats::GetBusyTimeNs() const {
        auto busyTime = active ? Elapsed(HighResClock::now(), mark) : NanoSecs(0);
        assert(busyTime >= NanoSecs(0), "Something is wrong. Please check clocks.");
        return busyTime;
    }
}
