#include "logger.hpp"
#include <stdexcept>
#include <memory>
#include <ctime>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <algorithm>
#include "utils.hpp"

namespace Sb {
    Logger* Logger::theLogger = nullptr;

    Logger::Logger() {
    }

    void Logger::Start() {
        if(Logger::theLogger != nullptr) {
            throw std::runtime_error("start called when already started");
        }
        Logger::theLogger = new Logger;
    }

    void Logger::DoSetMask(LogType mask) {
        logMask = mask;
    }

    void Logger::SetMask(LogType mask) {
        Logger::theLogger->DoSetMask(mask);
    }

    void Logger::Stop() {
        if(Logger::theLogger == nullptr) {
            throw std::runtime_error("stop called before start");
        }
        std::unique_ptr<Logger> tmp(Logger::theLogger);
        Logger::theLogger = nullptr;
    }

    void Logger::Log(LogType type, std::string what) {
        if(Logger::theLogger == nullptr) {
            throw std::runtime_error("need to call start first or start failed");
        }
        Logger::theLogger->DoLog(type, what);
    }
    void Logger::DoLog(LogType type, std::string what) {
        if(type >= logMask) {
            return;
        }
        auto now_ns = std::chrono::duration_cast<NanoSecs>(
                                HighResClock::now().time_since_epoch()).count();
        std::time_t secs = now_ns / NanoSecsInSecs;
        int64_t nsecs = now_ns % NanoSecsInSecs;

        lock.lock();
        std::tm* local_time_now = std::localtime(&secs);
        lock.unlock();

        std::locale loc;
        const std::time_put<char>& tmput = std::use_facet<std::time_put<char>>(loc);

        std::stringbuf line;
        std::ostream os(&line);
        std::string fmt("%Y-%Om-%Od %OH:%OM:%OS.");
        tmput.put(os, os, '.', local_time_now, fmt.data(), fmt.data() + fmt.length());
        os << nsecs;
        auto start = nsecs == 0 ? static_cast<int64_t>(1) : nsecs;
        for(auto div = start; div < 100000000; div *= 10) {
            os << "0";
        }

        lock.lock();
        std::size_t tid = std::numeric_limits<std::size_t>::max();
        for(std::size_t i = 0; i < Logger::threadIds.size(); i++) {
            if(threadIds.at(i) == std::this_thread::get_id()) {
                tid = i;
            }
        }
        if(tid == std::numeric_limits<std::size_t>::max()) {
            Logger::threadIds.push_back(std::this_thread::get_id());
            tid = Logger::threadIds.size() - 1;
        }
        lock.unlock();

        os << " " << std::setw(2) << tid << " ";
        os << what << "\n";

        lock.lock();
        std::cerr << line.str();
        lock.unlock();
    }
}
