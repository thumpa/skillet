#pragma once

namespace Sb {
    class Epollable {
    public:
        virtual ~Epollable();
        virtual void Run() = 0;
        virtual void Cancel();
        int GetFd() const;
    protected:
        int fd = -1;
    };
}
