#pragma once
#include "timer.hpp"

namespace Sb {
    class ExitTimer : virtual public Timer {
    public:
        ExitTimer(int64_t timeoutNanoSecs) : Timer(timeoutNanoSecs) {}
        void Run() {
            throw std::runtime_error("Throwing ExitTimer bye bye.");
        }
    };
}

