TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += ../../src
QMAKE_CXXFLAGS += -std=c++11 -fexceptions -Wall -Werror -ggdb -g
LIBS += -lpthread


SOURCES += \
    ../../test/main.cpp \
    ../../src/logger.cpp \
    ../../src/engine.cpp \
    ../../src/timer.cpp \
    ../../src/epollable.cpp \
    ../../src/stats.cpp

